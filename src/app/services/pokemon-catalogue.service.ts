import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonList} from '../models/pokemon.model';

const {apiPokemons, apiPokemonImage} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {
private _pokemons: Pokemon[] = [];
private _error: string = "";
private _loading: boolean = false;
private _image: string = ""

get pokemons(): Pokemon[] {
  return this._pokemons;
}

get image(): string {
  return this._image;
}

get error(): string{
  return this._error;
}

get loading(): boolean{
  return this._loading
}
  constructor(
    private readonly http: HttpClient
  ) { }

  public findAllPokemons(): void{
    if(this._pokemons.length > 0 || this.loading){
      return;
    }
    this._loading = true // load when the fetch request is active
    this.http.get<PokemonList>(apiPokemons)
    .pipe(
      finalize(() => {
        this._loading = false; // stop loading
      })
    )
    .subscribe({
      next: (pokemonList: PokemonList) => {        
        for (let i = 0; i < pokemonList.results.length; i++) {
          // loop the incomming data and create the pokemons like the interface: Pokemon
          this._pokemons.push({
            name: pokemonList.results[i].name,
            url: pokemonList.results[i].url,
            image: this.getPokemonImage(pokemonList.results[i])
          })
        }
      },
      error: (error: HttpErrorResponse) => { // this._error can be displayed via the get methods in other components
        this._error = error.message; // ... to read the error 
      }
    })
  }
  // find the pokemon though "name" from all the pokemons (_pokemons)
  public pokemonById(name: string): Pokemon| undefined{
    return this._pokemons.find((pokemon: Pokemon)=> pokemon.name === name);
  }
  // returns the src link to give it to the pokemons
  public getPokemonImage(pokemon: Pokemon){

      return `${apiPokemonImage}${this.getPokemonId(pokemon)}.png`
  }
  // returns only the id of the pokemon, if double numbers, else if only one number
  public getPokemonId(pokemon: Pokemon){
    if(pokemon.url.length == 37) {
      const a = pokemon.url[pokemon.url.length - 3]
      const b = pokemon.url[pokemon.url.length - 2]

      return a + b
    }
    else{

      return pokemon.url[pokemon.url.length - 2]
    }
    
  }
}
