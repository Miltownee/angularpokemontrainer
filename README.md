# PokemonTrainer

This is a webbapplication created in angular where you can sign in and capture your favourite pokemons. Capture pokemons will be displayed in the trainers page.

# Install 
Step 1: Clone the reposetory:
git clone https://gitlab.com/Miltownee/angularpokemontrainer.git

Step 2: Install angular cli:
npm install - g @angular/cli

Step 3: Run server and see your application in action:
ng-serve

Step 4: Run version check 
node --version
npm --version

# Usage 

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

# Contributors

@Miltownee @SanjinAjanic

# License

Copyright 2022, Experis