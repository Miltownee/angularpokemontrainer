import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';


const apiUsers = environment.apiTrainers;
const APIkey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private readonly http: HttpClient) { }
  // login user or call "create new user"
  public login(username: string): Observable<Trainer>{
    return this.checkUsername(username)
    .pipe(
      switchMap((user: Trainer | undefined) => {
        if (user === undefined){
          return this.createTrainer(username);
        }
        if(username == "ash"){ // to change the standard trainer on heroku to the model standards
          user.pokemon = []
          user.pokemon.push({
            name: "pikachu",
            url: "https://pokeapi.co/api/v2/pokemon/25/",
            image: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png"
          },
          {
            name: "bulbasaur",
            url: "https://pokeapi.co/api/v2/pokemon/1/",
            image: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
          })
        }
        return of(user)
      }),
    )
  }
  // check if user exist, returns user or "undefined" if user doesn't exist
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiUsers}?username=${username}`)
    .pipe(
      map((response: Trainer[]) => response.pop())
      
      )
    }
    // if not user - create user
    private createTrainer(username: string): Observable<Trainer> {
      const trainer = {
        username,
        pokemon: []
      };
      
      // RxJS operators
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": APIkey
    })
    // POST - Create items on the server
    return this.http.post<Trainer>(apiUsers, trainer, {
      headers
    })
  }
}
