import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { LandingPage } from './pages/landing/landing.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { AppRoutingModule } from './app-routing.module';
import { LandingFormComponent } from './components/landing-form/landing-form.component';
import { FormsModule } from '@angular/forms';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CapturedButtonComponent } from './components/captured-button/captured-button.component';

// Delorator
@NgModule({
  declarations: [// Components go in the declarations
    AppComponent,
    LandingPage,
    PokemonCataloguePage,
    TrainerPage,
    LandingFormComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    NavbarComponent,
    CapturedButtonComponent
  ],
  imports: [ // modules go in the imports
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
