import { Component, Input, OnInit } from '@angular/core';
import { Pokemon} from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {
  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService,
    private readonly trainerService: TrainerService
  ) { }

  get image(): string{
    return this.pokemonCatalogueService.image
  }

  get trainer(): any{
    return this.trainerService.trainer
  }

  @Input() pokemon?: Pokemon; // gets pokemons from pokemon-list


  ngOnInit(): void {
  }

}
