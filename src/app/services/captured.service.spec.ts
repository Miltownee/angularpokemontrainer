import { TestBed } from '@angular/core/testing';

import { CapturedService } from './captured.service';

describe('CapturedService', () => {
  let service: CapturedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CapturedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
