import { Pokemon} from "./pokemon.model";

// trainer/user with id, name and a array with all the aquired pokemons
export interface Trainer{
    id: number;
    username: string;
    pokemon: Pokemon[];
}