import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon} from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';
import { tap } from 'rxjs';

const {apiKey, apiTrainers: apiTrainers} = environment

@Injectable({
  providedIn: 'root'
})
export class CapturedService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService:PokemonCatalogueService,
    private readonly trainerService:TrainerService,
  ) { }

    // checks if pokemon is already acquired, else pushes it to addToCaptured func
    public addToCaptured(pokemonName:string): Observable<Trainer>{
      if(!this.trainerService.trainer) throw new Error("addToCaptured: There is no user")
      
      const user: Trainer = this.trainerService.trainer;
      const pokemon: Pokemon| undefined = this.pokemonService.pokemonById(pokemonName);

    if(!pokemon){
      throw new Error("addToCaptured: No pokemon with name: " + pokemonName)
    }

    if(this.trainerService.inCaptured(pokemonName)){
      this.trainerService.removeFromCaptured(pokemonName)
    }
    else{
      this.trainerService.addToCaptured(pokemon)
    }
    
    // PATCH - the trainer with the new pokemon[] list
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

      return this.http.patch<Trainer>(`${apiTrainers}/${user.id}`,{
        pokemon: [...user.pokemon] // Already updated
      },
      {
        headers
      })
      .pipe(
        tap((updatedUser: Trainer)=>{
          this.trainerService.trainer = updatedUser;
        }
        )
      )
    }

}
