import { Component, OnInit } from '@angular/core';
import { Pokemon} from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
// get the logged in trainer
get trainer(): Trainer | undefined {
  return this.userService.trainer;
}
// get the pokemons that the trainer currently have
get captured(): Pokemon[]{
  if(this.userService.trainer){
    return this.userService.trainer.pokemon
  }
  return[];
}

  constructor(
    private userService: TrainerService
  ) { }

  ngOnInit(): void {
  }

}
