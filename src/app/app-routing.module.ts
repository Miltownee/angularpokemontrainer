import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { LandingPage } from "./pages/landing/landing.page";
import { TrainerPage } from "./pages/trainer/trainer.page";
import { Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/landing"
    },
    {
        path: "landing",
        component: LandingPage
    },
    {
        path: "pokemons",
        component: PokemonCataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], // import a module
    exports: [
        RouterModule
    ] // Expose module and it's features
})
export class AppRoutingModule {

}