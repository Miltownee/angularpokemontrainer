import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CapturedService } from 'src/app/services/captured.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-captured-button',
  templateUrl: './captured-button.component.html',
  styleUrls: ['./captured-button.component.css']
})
export class CapturedButtonComponent implements OnInit {

  
  public loading: boolean = false
  public isCaptured: boolean = false;
  @Input() pokemonName: string = "";

  constructor(
    private userService: TrainerService,
    private readonly capturedService: CapturedService,
  ) { }

  // Is the pokemon already captured, boolean
  ngOnInit(): void {
    this.isCaptured = this.userService.inCaptured(this.pokemonName)
  }


  // add the chosen pokemon to captured pokemons
  onCapturedClick(): void{
    this.loading = true;
    this.capturedService.addToCaptured(this.pokemonName)
    .subscribe({
      next: (user: Trainer) => {
        this.loading = false
        this.isCaptured = this.userService.inCaptured(this.pokemonName)        
      },
      error: (error: HttpErrorResponse) =>{
        console.log("ERROR", error.message);
        
      } 
    })
  }

}
