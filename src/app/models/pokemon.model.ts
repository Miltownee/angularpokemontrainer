    // the pokemons that will be in the trainers pokemon[]
    export interface Pokemon {
        name: string;
        url: string;
        image: string;
    }
    // to get the fetch/observable from the pokeapi
    export interface PokemonList {
        count: number;
        next: string;
        previous: string;
        results: Pokemon[];
    }

   


