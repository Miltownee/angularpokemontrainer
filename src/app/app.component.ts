import { Component } from '@angular/core';
import { PokemonCatalogueService } from './services/pokemon-catalogue.service';
import { TrainerService } from './services/trainer.service';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private readonly userService: TrainerService,
    private readonly pokemonService: PokemonCatalogueService
  ){}
  title = 'angular-pokemon-trainer';
    // if trainer exist, get all the pokemons
  ngOnInit(): void{
    if(this.userService.trainer){
      this.pokemonService.findAllPokemons();
    }
  }
}
