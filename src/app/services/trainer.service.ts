import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon} from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

private _trainer?: Trainer;

get trainer(): Trainer | undefined {
  return this._trainer
}
// sets the trainer to sessionstorage
set trainer(user: Trainer | undefined){
  StorageUtil.storageSave<Trainer>(StorageKeys.User, user!)
  this._trainer = user;
}

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.User);
   
  }
  // checks if trainer already have this pokemon, returns bool
  public inCaptured(pokemonName: string): boolean{
    if(this._trainer){
      return Boolean(this.trainer?.pokemon.find((pokemon: Pokemon)=>pokemon.name === pokemonName))

    }
    return false;
  }
  // add the chosen pokemon to the trainer pokemon list
  public addToCaptured(pokemon: Pokemon): void{
    if(this._trainer){
      this._trainer.pokemon.push(pokemon)
    }
  }
  // removes the pokemon from the trainers pokemon array
  public removeFromCaptured(pokemonName: string): void{
    if(this._trainer){
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.name !== pokemonName)
    }
  }
}
