import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { Pokemon} from 'src/app/models/pokemon.model';
@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {
  // get pokemons to iterate with child
get pokemons(): Pokemon[]{
  return this.pokemonCatalogueService.pokemons
}
// get loading to set if loading is active
get loading(): boolean{
  return this.pokemonCatalogueService.loading
}
// get the error message 
get error(): string{
  return this.pokemonCatalogueService.error
}
  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }
    // GET - all the pokemons to pokemonCatalogueService
  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemons();
  }

}
