import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TrainerService } from '../services/trainer.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(
    private readonly router: Router,
    private readonly userService: TrainerService
  ){}
  // the authguard checks if trainer is active, if not it calls router to change the page to "landing"
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.userService.trainer){
      return true;

    }
    else{
      this.router.navigateByUrl("/landing")
      return false
    }
  }
  
}
