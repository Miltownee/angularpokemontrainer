import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { LandingService } from 'src/app/services/landing.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-landing-form',
  templateUrl: './landing-form.component.html',
  styleUrls: ['./landing-form.component.css']
})
export class LandingFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();


  constructor(
    private readonly landingService: LandingService,
    private readonly trainerService: TrainerService) { }

  // try to login trough landingService func "login"
  public landingSubmit(landingForm: NgForm): void{

    const {username} = landingForm.value

    this.landingService.login(username)
    .subscribe({
      next: (trainer : Trainer) => {
        this.trainerService.trainer = trainer
        this.login.emit();
      },
    error: () => {

    }
    })
  }

}
